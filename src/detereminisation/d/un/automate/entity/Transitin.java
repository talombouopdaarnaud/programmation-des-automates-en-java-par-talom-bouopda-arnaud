/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package detereminisation.d.un.automate.entity;

/**
 *
 * @author JESUS-CHRIST
 */
public class Transitin {

    private String etat_i;
    private String caract;
    private String etat_f;

    public Transitin() {
    }


    public Transitin(String etat_i, String caract, String etat_f) {
        this.etat_i = etat_i;
        this.caract = caract;
        this.etat_f = etat_f;
    }

    public String getEtat_i() {
        return etat_i;
    }

    public void setEtat_i(String etat_i) {
        this.etat_i = etat_i;
    }

    public String getCaract() {
        return caract;
    }

    public void setCaract(String caract) {
        this.caract = caract;
    }

    public String getEtat_f() {
        return etat_f;
    }

    public void setEtat_f(String etat_f) {
        this.etat_f = etat_f;
    }
        
}
